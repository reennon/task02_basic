package com.epam;

/**
 * Fibonacci - class to calculate the fibonacci num.
 */

public final class Fibonacci {

    /**
     * private Fibonacci - private constructor.
     */
    private Fibonacci() {
    }

    /**
     * getFibonacci - method to find the fibonacci number.
     *
     * @param number the const number that we want to know
     * @return fibonacci number
     */
    public static int getFibonacci(final int number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        return getFibonacci(number - 1) +
                getFibonacci(number - 2);
    }
}

